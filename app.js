const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fileUpload = require('express-fileupload');
const session = require('express-session');

const roleRouter = require('./routes/home/role/role-router');
const loginRouter = require('./routes/login/login-router');
const homeRouter = require('./routes/home/home-router');
const userRouter = require('./routes/home/user/user-router');
const divisionRouter = require('./routes/home/division/division-router');
const permissionRouter = require('./routes/home/permission/permission-router');
const deviceRouter = require('./routes/home/device/device-router');
const multimediaRouter = require('./routes/home/multimedia/multimedia-router');
const carouselRouter = require('./routes/home/carousel/carousel-router');
const carouselClientRouter = require('./routes/client/carousel-client-router');

let app = express();

app.use(fileUpload({
    createParentPath: true
}));

app.use(session({
    secret: '@fsanpablo.com',
    resave: true,
    saveUninitialized: true
}));

app.set('views', [
    path.join(__dirname, 'views'),
    path.join(__dirname, 'views/login/'),
    path.join(__dirname, 'views/home/'),
    path.join(__dirname, 'views/home/role/'),
    path.join(__dirname, 'views/home/user/'),
    path.join(__dirname, 'views/home/division/'),
    path.join(__dirname, 'views/home/device/'),
    path.join(__dirname, 'views/home/multimedia/'),
    path.join(__dirname, 'views/home/carousel/'),
    path.join(__dirname, 'views/client/')
]);
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/css', express.static(__dirname + '/node_modules/datatables.net-dt/css'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist/umd'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/datatables.net/js'));

app.use('/', loginRouter);
app.use('/login/', loginRouter);
app.use('/home/', homeRouter);
app.use('/home/roles/', roleRouter);
app.use('/home/users/', userRouter);
app.use('/home/divisions/', divisionRouter);
app.use('/home/permissions/', permissionRouter);
app.use('/home/devices/', deviceRouter);
app.use('/home/multimedia/', multimediaRouter);
app.use('/home/carousel/', carouselRouter);
app.use('/carousel/', carouselClientRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;