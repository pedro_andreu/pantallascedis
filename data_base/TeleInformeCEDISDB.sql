CREATE TABLE [ticedis].[dbo].[tbl_cat_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[creation_date] [datetime] NOT NULL DEFAULT CURRENT_TIMESTAMP,
	[modification_date] [datetime] NULL,
	[status] [bit] NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE [ticedis].[dbo].[tbl_cat_roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[creation_date] [datetime] NOT NULL DEFAULT CURRENT_TIMESTAMP,
	[modification_date] [datetime] NULL,
	[status] [bit] NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE [ticedis].[dbo].[tbl_rel_roles_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_role] [int] NOT NULL,
	[fk_permission] [int] NOT NULL,
    PRIMARY KEY (id),
	CONSTRAINT FK_role_permission FOREIGN KEY (fk_role)
    REFERENCES tbl_cat_roles(id),
	CONSTRAINT FK_permission_role FOREIGN KEY (fk_permission)
    REFERENCES tbl_cat_permissions(id)
);

CREATE TABLE [ticedis].[dbo].[tbl_cat_users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[fk_role] [int] NOT NULL,
	[creation_date] [datetime] NOT NULL DEFAULT CURRENT_TIMESTAMP,
	[modification_date] [datetime] NULL,
	[status] [bit] NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT FK_user_role FOREIGN KEY (fk_role)
    REFERENCES tbl_cat_roles(id)
);

CREATE TABLE [ticedis].[dbo].[tbl_cat_divisions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[creation_date] [datetime] NOT NULL DEFAULT CURRENT_TIMESTAMP,
	[modification_date] [datetime] NULL,
	[status] [bit] NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE [ticedis].[dbo].[tbl_cat_devices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_division] [int] NOT NULL,
	[ip] [varchar](15) NOT NULL,
	[creation_date] [datetime] NOT NULL DEFAULT CURRENT_TIMESTAMP,
	[modification_date] [datetime] NULL,
	[status] [bit] NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT FK_device_division FOREIGN KEY (fk_division)
    REFERENCES tbl_cat_divisions(id)
);

CREATE TABLE [ticedis].[dbo].[tbl_cat_multimedia_files](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[type] [varchar](50) NOT NULL,
	[format] [varchar](50) NOT NULL,
	[mimetype] [varchar](50) NOT NULL,
	[size] [varchar](50) NOT NULL,
	[link] [varchar](100) NOT NULL,
	[creation_date] [datetime] NOT NULL DEFAULT CURRENT_TIMESTAMP,
	[modification_date] [datetime] NULL,
	[status] [bit] NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE [ticedis].[dbo].[tbl_cat_carousel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[position] [int] NOT NULL,
	[interval] [varchar](10) NOT NULL,
	[fk_multimedia] [int] NOT NULL,
	[fk_division] [int] NOT NULL,
	[creation_date] [datetime] NOT NULL DEFAULT CURRENT_TIMESTAMP,
	[modification_date] [datetime] NULL,
	[status] [bit] NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT FK_multimedia_file FOREIGN KEY (fk_multimedia)
    REFERENCES [ticedis].[dbo].[tbl_cat_multimedia_files](id),
	CONSTRAINT FK_division FOREIGN KEY (fk_division)
    REFERENCES [ticedis].[dbo].[tbl_cat_divisions](id)
);

INSERT INTO [dbo].[tbl_cat_permissions]
           ([name]
           ,[status])
     VALUES
           ('Roles'
		   ,1),
		   ('Usuarios'
		   ,1),
		   ('Divisiones'
		   ,1),
		   ('Dispositivos'
		   ,1),
		   ('Multimedia'
		   ,1),
		   ('Carousel'
		   ,1)