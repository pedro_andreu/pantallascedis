const sql = require('mssql');

let Connection = require('../tools/connection.js');
const DATA = require('../tools/data.js');

const CONFIGDB = Connection.config();

const SQL_ALL = "SELECT id, name FROM tbl_cat_permissions WHERE status = @status";
const SQL_SEARCH_PERMISSION_BY_USER = "SELECT P.name FROM tbl_cat_users U INNER JOIN tbl_cat_roles R ON R.id = U.fk_role INNER JOIN tbl_rel_roles_permissions RP ON R.id = RP.fk_role INNER JOIN tbl_cat_permissions P ON P.id = RP.fk_permission WHERE U.id = @id AND R.status = @status";

module.exports = class Permission {

    async getPermissions() {
        let permissions;
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request()
                .input('status', sql.Bit, DATA.ACTIVE)
                .query(SQL_ALL);
            if (result !== null) {
                return result.recordset;
            } else {
                throw new Error("No se han encontrado resultados");
            }
        } catch (err) {
            sql.close();
        }
    }

    async findPermissionByUserId(id) {
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request()
                .input('id', sql.Int, id)
                .input('status', sql.Bit, DATA.ACTIVE)
                .query(SQL_SEARCH_PERMISSION_BY_USER);
            if (result !== null) {
                return result.recordset;
            } else {
                throw new Error("No se han encontrado resultados");
            }
        } catch (err) {
            sql.close();
        }
    }

};