const sql = require('mssql');

let Connection = require('../tools/connection.js');
const DATA = require('../tools/data.js');

const CONFIGDB = Connection.config();

const SQL_ALL = "SELECT id, name, FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_roles";
const SQL_CREATE = "INSERT INTO tbl_cat_roles (name, status) VALUES (@name, @status);SELECT SCOPE_IDENTITY() AS id;";
const SQL_CREATE_REL = 'INSERT INTO tbl_rel_roles_permissions (fk_role, fk_permission) VALUES (@fk_role, @fk_permission)';
const SQL_FIND_BY_ID = 'SELECT id, name, status FROM tbl_cat_roles WHERE id = @id';
const SQL_FIND_PERMISSION_BY_ID_ROLE = 'SELECT P.id, P.name FROM tbl_cat_roles R INNER JOIN tbl_rel_roles_permissions RP ON R.id = RP.fk_role INNER JOIN tbl_cat_permissions P ON P.id = RP.fk_permission WHERE R.id = @id';

module.exports = class Role {

    async getRoles() {
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request().query(SQL_ALL);
            if (result !== null) {
                sql.close();
                return result.recordset;
            } else {
                sql.close();
                throw new Error(DATA.NO_RESULT_MESSAGE);
            }
        } catch (err) {
            sql.close();
            throw new Error(DATA.NO_RESULT_MESSAGE);
        }
    }

    async createRole(name, status) {
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request()
                .input('name', sql.VarChar, name)
                .input('status', sql.Int, status)
                .query(SQL_CREATE);
            if (result !== null) {
                let id;
                result.recordset.forEach(function(value, indice, array) {
                    id = value.id;
                });
                return id;
            } else {
                sql.close();
                throw new Error(DATA.NO_TRANSACTION_MESSAGE);
            }
        } catch (err) {
            sql.close();
            throw new Error(DATA.NO_TRANSACTION_MESSAGE);
        }
    }

    async createRolePermission(id_role, permissions) {
        try {
            let pool = await sql.connect(CONFIGDB);
            for (let i = 0; i < permissions.length; i++) {
                let result = await pool.request()
                    .input('fk_role', sql.Int, id_role)
                    .input('fk_permission', sql.Int, permissions[i])
                    .query(SQL_CREATE_REL);
            }
        } catch (err) {
            sql.close();
            throw new Error(DATA.NO_TRANSACTION_MESSAGE);
        }
    }

    async findRoleById(id) {
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request()
                .input('id', sql.Int, id)
                .query(SQL_FIND_BY_ID);
            if (result !== null) {
                sql.close();
                return result.recordset;
            } else {
                sql.close();
                throw new Error(DATA.NO_RESULT_MESSAGE);
            }
        } catch (err) {
            sql.close();
            throw new Error(DATA.NO_RESULT_MESSAGE);
        }
    }

    async findPermissionsByRole(id) {
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request()
                .input('id', sql.Int, id)
                .query(SQL_FIND_PERMISSION_BY_ID_ROLE);
            if (result !== null) {
                sql.close();
                return result.recordset;
            } else {
                sql.close();
                throw new Error(DATA.NO_RESULT_MESSAGE);
            }
        } catch (err) {
            sql.close();
            throw new Error(DATA.NO_RESULT_MESSAGE);
        }
    }

};