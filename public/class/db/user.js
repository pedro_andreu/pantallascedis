const sql = require('mssql');

let Connection = require('../tools/connection.js');
const DATA = require('../tools/data.js');

const CONFIGDB = Connection.config();

const SQL_FIND_USER_BY_ID = 'SELECT U.id AS id, U.name AS name, U.first_name AS first_name, U.last_name AS last_name, R.name AS role FROM tbl_cat_users U INNER JOIN tbl_cat_roles R ON R.id = U.fk_role WHERE U.id = @id AND U.status = @ustatus AND R.status = @rstatus';
const SQL_FIND_USER_BY_EMAIL = 'SELECT id FROM tbl_cat_users WHERE email = @email AND status = @status';

module.exports = class User {

    async findUserById(id) {
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request()
                .input('id', sql.Int, id)
                .input('ustatus', sql.Bit, DATA.ACTIVE)
                .input('rstatus', sql.Bit, DATA.ACTIVE)
                .query(SQL_FIND_USER_BY_ID);
            if (result !== null) {
                sql.close();
                return result.recordset;
            } else {
                sql.close();
                throw new Error(DATA.NO_RESULT_MESSAGE);
            }
        } catch (err) {
            sql.close();
            throw new Error(DATA.NO_RESULT_MESSAGE);
        }
    }

    async findUserByEmail(email) {
        try {
            let pool = await sql.connect(CONFIGDB);
            let result = await pool.request()
                .input('email', sql.VarChar, email)
                .input('status', sql.Bit, DATA.ACTIVE)
                .query(SQL_FIND_USER_BY_EMAIL);
            if (result !== null) {
                sql.close();
                return result.recordset;
            } else {
                sql.close();
                throw new Error(DATA.NO_RESULT_MESSAGE);
            }
        } catch (err) {
            sql.close();
            throw new Error(DATA.NO_RESULT_MESSAGE);
        }
    }

};