module.exports = class Permission {

    id;
    name;
    creation_date;
    modification_date;
    status;

    constructor(id, name, creation_date, modification_date, status) {
        this.id = id;
        this.name = name;
        this.creation_date = creation_date;
        this.modification_date = modification_date;
        this.status = status;
    }

    get id() {
        return this.id;
    }

    set id(id) {
        this.id = id;
    }

    get name() {
        return this.name;
    }

    set name(name) {
        this.name = name;
    }

    get creation_date() {
        return this.creation_date;
    }

    set creation_date(creation_date) {
        this.creation_date = creation_date;
    }

    get modification_date() {
        return this.modification_date;
    }

    set modification_date(modification_date) {
        this.modification_date = modification_date;
    }

    get status() {
        return this.status;
    }

    set status(status) {
        this.status = status;
    }

}