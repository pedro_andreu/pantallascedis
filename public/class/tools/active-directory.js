const soapRequest = require('easy-soap-request');
const parseString = require('xml2js').parseString;

const DATA = require('../tools/data.js');

module.exports = class ACtiveDirectory {

    async findUser(email, password) {
        //console.log(email);
        //console.log(password);
        try {
            const URL = 'http://192.168.0.219:98/Perfiles.svc';
            const HEADERS = {
                'user-agent': 'Consulta-Active-Directory',
                'Content-Type': 'text/xml;charset=UTF-8',
                'soapAction': 'http://tempuri.org/IPerfiles/ConexionAD',
            };
            const XML = await `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"><soapenv:Header/><soapenv:Body><tem:ConexionAD><tem:Nombre>${email}</tem:Nombre><tem:contraseña>${password}</tem:contraseña></tem:ConexionAD></soapenv:Body></soapenv:Envelope>`;
            let { response } = await soapRequest({ url: URL, headers: HEADERS, xml: XML, timeout: 1000 });
            let { headers, body, statusCode } = response;
            //console.log(statusCode);
            //console.log(body);
            if (statusCode === 200 && body.indexOf('<ConexionADResult>0</ConexionADResult>') >= 0) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            console.log(err);
            throw new Error(DATA.NO_RESULT_MESSAGE);
        }
    }

};