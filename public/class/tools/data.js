module.exports = class Data {

    static OK = 200;
    static CREATED = 201;
    static NO_CONTENT = 204;
    static BAD_REQUEST = 400;
    static UNAUTHORIZED = 401;
    static INTERNAL_SERVER_ERROR = 500;
    static ERROR_MESSAGE = 'Se ha generado un error en la aplicación, contacte al administrador del sistema';
    static NO_RESULT_MESSAGE = 'No se han encontrado resultados';
    static NO_TRANSACTION_MESSAGE = 'No se ha podido realizar la transacción';
    static CREATED_ROLE_MESSAGE = 'Se ha creado el nuevo rol';
    static HOME_VIEW = 'home-view';
    static LOGIN_VIEW = 'login-view';
    static ROLE_VIEW = 'role-view';




    static VIEW_ROLE = 'role-view';
    static VIEW_USER = 'user-view';
    static VIEW_DIVISION = 'divisions-view';
    static VIEW_DEVICE = 'device-view';
    static ACTIVE = true;
    static DEACTIVE = false;

};