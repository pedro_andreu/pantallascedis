$(document).ready(function() {
    $('#tableContent').DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "No se han encontrado registros",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty": "Mostrando 0 a 0 de 0 registros",
            "infoFiltered": "(filtrado de _MAX_ total registros)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Ver _MENU_ registros",
            "loadingRecords": "Loading...",
            "processing": "Processing...",
            "search": "Parámetros de búsqueda:",
            "zeroRecords": "No se han encontrado resultados",
            "paginate": {
                "first": "First",
                "last": "Last",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    });
});