var months = new Array ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                       "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
                       "Diciembre");
var days = new Array("Domingo", "Lunes", "Martes", "Miércoles",
                           "Jueves", "Viernes", "Sábado");
var date = new Date();
document.write("<p class='col-form-label-sm m-1'>" + days[date.getDay()] + ", " + 
               date.getDate() + " de " + months[date.getMonth()] + " de " + 
               date.getFullYear() + "</p>");