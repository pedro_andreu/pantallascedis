const PORT = 3000;
const SERVER = 'http://localhost:' + PORT;

$('#btn-permissions').click(function() {
    let tbody_permissions = $('#tbody-permissions').html('');
    fetch(SERVER + '/home/permissions/')
        .then(resp => resp.json())
        .then(result => {
            result.permissions.forEach(function(value, indice, array) {
                tbody_permissions.append(
                    '<tr>' +
                    '<td class="col-form-label-sm">' +
                    '<div class="custom-control custom-switch input-group-sm">' +
                    '<input type="checkbox" id="permission' + value.id + '" name="permission" value="' + value.id + '" class="custom-control-input">' +
                    '<label class="custom-control-label" for="permission' + value.id + '"></label>' +
                    '</div>' +
                    '</td>' +
                    '<td class="col-form-label-sm">' +
                    value.name +
                    '</td>' +
                    '</tr>');
            });
        });
});

$('#showPermissions').on('show.bs.modal', function(event) {
    let button = $(event.relatedTarget);
    let id = button.data('whatever');
    fetch(SERVER + '/home/permissions/search?id=' + id)
        .then(resp => resp.json())
        .then(respObj => {
            let ul_permissions = $('#ul-permissions').html('');
            let err = $('#no-result-alert').html('');
            if (respObj.permissions.length > 0) {
                respObj.permissions.forEach(function(value, indice, array) {
                    ul_permissions.append('<li class="list-group-item">' + value.name + '</li>');
                });
            } else {
                err.append('<div class="mt-3 text-center alert alert-warning alert-dismissible fade show" role="alert"><strong>El rol no tiene permisos</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            }
        });
});

$('#editRoles').on('show.bs.modal', function(event) {
    let button = $(event.relatedTarget);
    let id = button.data('whatever');
    fetch(SERVER + '/home/roles/search?id=' + id)
        .then(resp => resp.json())
        .then(respObj => {
            console.log(respObj.role);
            console.log(respObj.permissionsrel);
            respObj.role.forEach(function(value, indice, array) {
                $('#id').val(value.id);
                $('#name').val(value.name);
                $('#switch-active').val(value.status);
                if (value.status) {
                    $('#switch-active').prop("checked", true);
                }
            });
            respObj.permissionsrel.forEach(function(val, ind, arr) {
                respObj.permissions.forEach(function(v, i, a) {
                    if (v.id == val.id) {
                        respObj.permissions.splice(i, 1);
                    }
                });
            });
            let tablepermissions = $('#table-per').html('');
            respObj.permissionsrel.forEach(function(val, ind, arr) {
                tablepermissions.append('<tr>' +
                    '<td class="col-form-label-sm">' +
                    '<div class="custom-control custom-switch input-group-sm">' +
                    '<input type="checkbox" id="permission' + val.id + '" name="permission' + val.id + '" value="' + val.id + '" class="custom-control-input" checked>' +
                    '<label class="custom-control-label" for="permission' + val.id + '"></label>' +
                    '</div></td><td class="col-form-label-sm">' + val.name + '</td></tr>');
            });
            respObj.permissions.forEach(function(val, ind, arr) {
                tablepermissions.append('<tr>' +
                    '<td class="col-form-label-sm">' +
                    '<div class="custom-control custom-switch input-group-sm">' +
                    '<input type="checkbox" id="permission' + val.id + '" name="permission' + val.id + '" value="' + val.id + '" class="custom-control-input">' +
                    '<label class="custom-control-label" for="permission' + val.id + '"></label>' +
                    '</div></td><td class="col-form-label-sm">' + val.name + '</td></tr>');
            });
        });
});

$('#editUser').on('show.bs.modal', function(event) {
    let button = $(event.relatedTarget);
    let id = button.data('whatever');
    let fk;
    fetch(SERVER + '/home/users/search?id=' + id)
        .then(resp => resp.json())
        .then(respObj => {
            respObj.data.forEach(function(value, indice, array) {
                $('#id').val(value.id);
                $('#name').val(value.name);
                $('#first_name').val(value.first_name);
                $('#last_name').val(value.last_name);
                $('#email').val(value.email);
                $('#switch-active').val(value.status);
                if (value.status) {
                    $('#switch-active').prop("checked", true);
                }
                fk = value.fk_role;
            });
            let select = $('#role').html('');
            respObj.roles.forEach(function(val, ind, arr) {
                if (val.id == fk) {
                    select.append('<option selected value="' + val.id + '"> ' + val.name + '</option>');
                } else {
                    select.append('<option value="' + val.id + '"> ' + val.name + '</option>');
                }
            });
        });
});

$('#editDivision').on('show.bs.modal', function(event) {
    let button = $(event.relatedTarget);
    let id = button.data('whatever');
    fetch(SERVER + '/home/divisions/search?id=' + id)
        .then(resp => resp.json())
        .then(respObj => {
            respObj.data.forEach(function(value, indice, array) {
                $('#id').val(value.id);
                $('#name').val(value.name);
                $('#switch-active').val(value.status);
                if (value.status) {
                    $('#switch-active').prop("checked", true);
                }
            });
        });
});

$('#editDevice').on('show.bs.modal', function(event) {
    let button = $(event.relatedTarget);
    let id = button.data('whatever');
    let fk;
    fetch(SERVER + '/home/devices/search?id=' + id)
        .then(resp => resp.json())
        .then(respObj => {
            respObj.data.forEach(function(value, indice, array) {
                $('#id').val(value.id);
                $('#ip').val(value.ip);
                fk = value.fk_division;
                if (value.status) {
                    $('#switch-active').prop("checked", true);
                }
            });
            let select = $('#division').html('');
            respObj.divisions.forEach(function(val, ind, arr) {
                if (val.id == fk) {
                    select.append('<option selected value="' + val.id + '"> ' + val.name + '</option>');

                } else {
                    select.append('<option value="' + val.id + '"> ' + val.name + '</option>');
                }
            });
        });
});

$('#editMultimedia').on('show.bs.modal', function(event) {
    let button = $(event.relatedTarget);
    let id = button.data('whatever');
    fetch(SERVER + '/home/multimedia/search?id=' + id)
        .then(resp => resp.json())
        .then(respObj => {
            respObj.data.forEach(function(value, indice, array) {
                $('#id').val(value.id);
                if (value.name.indexOf('.') > -1) {
                    $('#name').val(value.name.split('.').slice(0, -1).join('.'));
                } else {
                    $('#name').val(value.name);
                }
                $('#customLabelLang').html(value.link);
                if (value.status) {
                    $('#switch-active').prop("checked", true);
                }
            });
        });
});

$('#editCarousel').on('show.bs.modal', function(event) {
    let button = $(event.relatedTarget);
    let recipient = button.data('whatever');
    let fk_division;
    let fk_multimedia;
    fetch(SERVER + '/home/carousel/search?id=' + recipient)
        .then(resp => resp.json())
        .then(respObj => {
            respObj.data.forEach(function(value, indice, array) {
                $('#id').val(value.id);
                fk_division = value.fk_division;
                fk_multimedia = value.fk_multimedia;
                $('#interval').val(value.interval);
                if (value.status) {
                    $('#switch-active').prop("checked", true);
                }
            });
            let selectD = $('#division').html('');
            respObj.divisions.forEach(function(val, ind, arr) {
                if (val.id == fk_division) {
                    selectD.append('<option selected value="' + val.id + '"> ' + val.name + '</option>');

                } else {
                    selectD.append('<option value="' + val.id + '"> ' + val.name + '</option>');
                }
            });
            let selectM = $('#multimedia').html('');
            respObj.multimedias.forEach(function(v, i, a) {
                if (v.id == fk_multimedia) {
                    selectM.append('<option selected value="' + v.id + '"> ' + v.name + '</option>');

                } else {
                    selectM.append('<option value="' + v.id + '"> ' + v.name + '</option>');
                }
            });
        });
});

function IsValisMultimediaFile() {
    let fileInput = document.getElementById('multimediaFile');
    let filePath = fileInput.value;
    let allowedExtensions = /(.jpg|.jpeg|.png|.gif|.mp4|.avi|.mkv)$/i;
    if (!allowedExtensions.exec(filePath)) {
        fileInput.value = '';
        return false;
    } else {
        return true;
    }
}

function IsValisMultimediaFileUpload() {
    let fileInput = document.getElementById('customFileLang');
    let filePath = fileInput.value;
    let allowedExtensions = /(.jpg|.jpeg|.png|.gif|.mp4|.avi|.mkv)$/i;
    if (!allowedExtensions.exec(filePath)) {
        fileInput.value = '';
        return false;
    } else {
        return true;
    }
}

$('#selectId').change("error", function(error) {
    var value = $(this).val();
    $("#createCarousel").prop("disabled", true);
    if (document.getElementById('selectId').value != '') {
        var id = document.getElementById('selectId').value;
        fetch(SERVER + '/home/carousel/search/division?id=' + id)
            .then(resp => resp.json())
            .then(respObj => {
                if (respObj.data.length != 0) {
                    fetch(SERVER + '/home/carousel/search/division/id/?id=' + id)
                        .then(resp => resp.json())
                        .then(carouselObj => {
                            let divMultimedia = $('#div-multimedia').html('');
                            carouselObj.carousels.forEach(function(value, indice, array) {
                                divMultimedia.append('<div id="vm' + value.id + 's" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">' +
                                    '<div class="row">' +
                                    '<div class="col-md-3">' +
                                    '<label class="col-form-label col-form-label-sm" for="name">' +
                                    'Archivo multimedia:' +
                                    '</label>' +
                                    '<select id="select-multimedia' + value.id + '" name="multimedia" class="custom-select custom-select-sm" required>' +
                                    '<option value="">....</option>' +
                                    '</select>' +
                                    '<div class="invalid-tooltip">' +
                                    'Seleccione el archivo multimedia' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="col-md-3">' +
                                    '<label class="col-form-label col-form-label-sm" for="name">' +
                                    'Intervalo de tiempo:' +
                                    '</label>' +
                                    '<input class="form-control form-control-sm" type="number" name="interval" value="' + value.interval + '" pattern="[0-9]+" minlength="1" maxlength="10" required>' +
                                    '<div class="invalid-tooltip">' +
                                    'Ingrese el Intervalo de tiempo' +
                                    '</div>' +
                                    '</div>' +
                                    '<div id="previewMultimedia' + value.id + '">' +
                                    '</div>' +
                                    '<div class="col-md-1 align-items-center justify-content-center d-flex">' +
                                    '<button type="button" class="close" name="' + value.id + 's" aria-label="Close" onclick="deleteMultimedia(this)" >' +
                                    '<span aria-hidden="true">&times;</span>' +
                                    '</button>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div><br />');
                                let selectM = $('#select-multimedia' + value.id);
                                let selectPM = $('#previewMultimedia' + value.id);
                                let fk = value.fk_multimedia;
                                carouselObj.multimedias.forEach(function(v, i, a) {
                                    if (v.id == fk) {
                                        selectM.append('<option selected value="' + v.id + '"> ' + v.name + '</option>');
                                        console.log("Video: " + v.type);
                                        console.log(v.link);
                                        if (v.type == 'imagen') {
                                            selectPM.append('<img class="previewMultimedia zoom p-1" src="' + v.link + '">');
                                        } else if (v.type == 'video') {
                                            selectPM.append('<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="' + v.link + '" allowfullscreen></iframe></div>');
                                        } else {
                                            selectPM.append('<img class="previewMultimedia" src="/images/preview.png">');
                                        }
                                    } else {
                                        selectM.append('<option value="' + v.id + '"> ' + v.name + '</option>');
                                    }
                                });
                                selectM.change("error", function(error) {
                                    var value = $(this).val();
                                    fetch(SERVER + '/home/multimedia/search?id=' + value)
                                        .then(resp => resp.json())
                                        .then(respObj => {
                                            var previewMultimediaId = selectPM.html('');
                                            if (respObj.data[0].type == 'imagen') {
                                                previewMultimediaId
                                                    .append('<img class="previewMultimedia" src="' + respObj.data[0].link + '">');
                                            } else if (respObj.data[0].type == 'video') {
                                                previewMultimediaId
                                                    .append('<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="' + respObj.data[0].link + '" allowfullscreen></iframe></div>');
                                            } else {
                                                previewMultimediaId
                                                    .append('<img class="previewMultimedia" src="/images/preview.png">');
                                            }
                                        });
                                });
                            });
                            $('#customSwitch6').prop("checked", true);
                        }).catch((error) => {
                            console.log("la division ya tiene un carousel");
                            var err = $('#errorAlert').html('');
                            err.append('<div class="text-center alert alert-danger alert-dismissible fade show" role="alert"><strong>Error al consultar el carousel</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        });
                } else {
                    let divMultimedia = $('#div-multimedia').html('');
                    divMultimedia.append('<div id="vm0" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">' +
                        '<div class="row">' +
                        '<div class="col-md-3">' +
                        '<label class="col-form-label col-form-label-sm" for="name">' +
                        'Archivo multimedia:' +
                        '</label>' +
                        '<select id="select-multimedia0" name="multimedia" class="custom-select custom-select-sm" required>' +
                        '<option value="">....</option>' +
                        '</select>' +
                        '<div class="invalid-tooltip">' +
                        'Seleccione el archivo multimedia' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-3">' +
                        '<label class="col-form-label col-form-label-sm" for="name">' +
                        'Intervalo de tiempo:' +
                        '</label>' +
                        '<input class="form-control form-control-sm" type="number" name="interval" value="" pattern="[0-9]+" minlength="1" maxlength="10" required>' +
                        '<div class="invalid-tooltip">' +
                        'Ingrese el Intervalo de tiempo' +
                        '</div>' +
                        '</div>' +
                        '<div id="previewMultimedia0">' +
                        '</div>' +
                        '</div>' +
                        '</div><br />');
                    var selectmultimedia = $('#select-multimedia0');
                    let selectPMv = $('#previewMultimedia0');
                    console.log();
                    respObj.multimedia.forEach(function(v1, i1, a2) {
                        selectmultimedia.append('<option value="' + v1.id + '" name="">' + v1.name + ' </optional>');
                    });
                    selectmultimedia.change("error", function(error) {
                        var value = $(this).val();
                        fetch(SERVER + '/home/multimedia/search?id=' + value)
                            .then(resp => resp.json())
                            .then(respObj => {
                                var previewMultimediaId = selectPMv.html('');
                                if (respObj.data[0].type == 'imagen') {
                                    previewMultimediaId
                                        .append('<img class="previewMultimedia" src="' + respObj.data[0].link + '">');
                                } else if (respObj.data[0].type == 'video') {
                                    previewMultimediaId
                                        .append('<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="' + respObj.data[0].link + '" allowfullscreen></iframe></div>');
                                } else {
                                    previewMultimediaId
                                        .append('<img class="previewMultimedia" src="/images/preview.png">');
                                }
                            });
                    });
                }
                $('#customSwitch6').prop("checked", false);
            }).catch((error) => {
                console.log("la division ya tiene un carousel");
                var err = $('#errorAlert').html('');
                err.append('<div class="text-center alert alert-danger alert-dismissible fade show" role="alert"><strong>Error al consultar el carousel</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            });
    }
});

function deleteMultimedia(select) {
    let name = select.attributes.name.value;
    $("#vm" + name).remove();
}


var count_click = 0;
$('#btn-multimedia').click(function() {
    var click = count_click += 1;
    var divMultimedia = $('#div-multimedia');
    fetch(SERVER + '/home/multimedia/search/all')
        .then(resp => resp.json())
        .then(respObj => {
            divMultimedia.append('<div id="vm' + click + '" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">' +
                '<div class="row">' +
                '<div class="col-md-3">' +
                '<label class="col-form-label col-form-label-sm" for="name">' +
                'Archivo multimedia:' +
                '</label>' +
                '<select id="select-multimediaAdd' + click + '" name="multimedia" class="custom-select custom-select-sm" required>' +
                '<option value="">....</option>' +
                '</select>' +
                '<div class="invalid-tooltip">' +
                'Seleccione el archivo multimedia' +
                '</div>' +
                '</div>' +
                '<div class="col-md-3">' +
                '<label class="col-form-label col-form-label-sm" for="name">' +
                'Intervalo de tiempo:' +
                '</label>' +
                '<input class="form-control form-control-sm" type="number" name="interval" pattern="[0-9]+" minlength="1" maxlength="10" required>' +
                '<div class="invalid-tooltip">' +
                'Ingrese el Intervalo de tiempo' +
                '</div>' +
                '</div>' +
                '<div id="previewMultimediaId' + click + '">' +
                '</div>' +
                '<div class="col-md-1 align-items-center justify-content-center d-flex">' +
                '<button type="button" class="close" name="' + click + '" aria-label="Close" onclick="deleteMultimedia(this)" >' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                '</div>' +
                '</div>' +
                '</div>');
            var selectmultimedia = $('#select-multimediaAdd' + click);
            respObj.data.forEach(function(value, indice, array) {
                selectmultimedia.append('<option value="' + value.id + '" name="' + click + '">' + value.name + ' </optional>');
            });

            $('#select-multimediaAdd' + click).change("error", function(error) {
                var value = $(this).val();
                fetch(SERVER + '/home/multimedia/search?id=' + value)
                    .then(resp => resp.json())
                    .then(respObj => {
                        var previewMultimediaId = $('#previewMultimediaId' + click).html('');
                        if (respObj.data[0].type == 'imagen') {
                            previewMultimediaId
                                .append('<img class="previewMultimedia" src="' + respObj.data[0].link + '">');
                        } else if (respObj.data[0].type == 'video') {
                            previewMultimediaId
                                .append('<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="' + respObj.data[0].link + '" allowfullscreen></iframe></div>');
                        } else {
                            previewMultimediaId
                                .append('<img class="previewMultimedia" src="/images/preview.png">');
                        }
                    });
            });
        });
});

$('#select-multimedia0').change("error", function(error) {
    var value = $(this).val();
    fetch(SERVER + '/home/multimedia/search?id=' + value)
        .then(resp => resp.json())
        .then(respObj => {
            var previewMultimediaId = $('#previewMultimediaId0');
            $(".previewMultimedia0").remove();
            if (respObj.data[0].type == 'imagen') {
                previewMultimediaId
                    .append('<img id="previewMultimedia0" class="previewMultimedia0" src="' + respObj.data[0].link + '">');
            } else if (respObj.data[0].type == 'video') {
                previewMultimediaId
                    .append('<div id="previewMultimedia0" class="embed-responsive embed-responsive-16by9 previewMultimedia0"><iframe class="embed-responsive-item" src="' + respObj.data[0].link + '" allowfullscreen></iframe></div>');
            } else {
                previewMultimediaId
                    .append('<img id="previewMultimedia0" class="previewMultimedia0" src="/images/preview.png">');
            }
        });
});

$('#carouselExampleIndicators').on('slid.bs.carousel', function(e) {
    let vi = $('#carouselExampleIndicators .carousel-inner .carousel-item.active video').first();
    if (vi.prop("tagName") === "VIDEO") {
        vi.get(0).play();
    }
});

$('#carouselExampleIndicators').bind('slide.bs.carousel', function(e) {
    let vf = $('#carouselExampleIndicators .carousel-inner .carousel-item.active video').first();
    if (vf.prop("tagName") === "VIDEO") {
        vf.get(0).pause();
        vf.get(0).currentTime = 0;
    }
});