var express = require('express');
var router = express.Router();

var sql = require('mssql');

sql.on('error', err => {
    console.log(err);
});

var config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};

router.get('/', (req, res) => {
    var ip = formatIP(req.connection.remoteAddress);
    console.log(ip);
    sql.connect(config).then(pool => {
        return pool.request()
            .input('ip', sql.VarChar, ip)
            .query('SELECT C.position, C.interval, M.link, M.type, M.name, M.mimetype FROM tbl_cat_devices DEV INNER JOIN tbl_cat_divisions DIV ON DIV.id = DEV.fk_division INNER JOIN tbl_cat_carousel C ON DIV.id = C.fk_division INNER JOIN tbl_cat_multimedia_files M ON M.id = C.fk_multimedia WHERE ip = @ip AND C.status = 1 AND DEV.status = 1');
    }).then(result => {
        console.log(result.recordset);
        if (result.recordset.length != 0) {
            res.status(200);
            res.render('carousel-client-view', { data: result.recordset, code: 200 });
        } else {
            res.status(200);
            res.render('carousel-client-view', { data: result.recordset, code: 200, message: 'No se han encontrado resultados' });
        }
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('carousel-client-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

function formatIP(ip) {
    if (ip.indexOf(":") >= 0) {
        ip = ip.replace(/^.*:/, '');
    }
    return ip;
}

module.exports = router;