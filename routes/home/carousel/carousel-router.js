var express = require('express');
var router = express.Router();

let DATA = require('../../../public/class/tools/data.js');

var sql = require('mssql');
var ip = require('ip');

sql.on('error', err => {
    console.log(err);
});

var config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};

var auth = (req, res, next) => {
    if (req.session && req.session.user !== null && req.session.admin) {
        return next();
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.VIEW_LOGIN, { code: DATA.UNAUTHORIZED });
    }
};

router.get('/', auth, (req, res) => {
    sql.connect(config).then(pool => {
        return pool.request().query("SELECT C.id AS id, D.name AS division, C.position AS position, C.interval AS interval, M.name AS name, M.link AS link, M.type AS type, M.id AS multimedia, FORMAT(C.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(C.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(C.status=1,'Activo','Inactivo') AS status FROM tbl_cat_carousel C INNER JOIN tbl_cat_multimedia_files M ON M.id = C.fk_multimedia INNER JOIN tbl_cat_divisions D ON D.id = C.fk_division");
    }).then(carousels => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT fk_division FROM tbl_cat_carousel GROUP BY fk_division");
        }).then(count => {
            sql.connect(config).then(pool => {
                return pool.request().query("SELECT id, name FROM tbl_cat_divisions WHERE status = 1");
            }).then(divisions => {
                sql.connect(config).then(pool => {
                    return pool.request().query("SELECT id, name, type, link FROM tbl_cat_multimedia_files WHERE status = 1");
                }).then(multimedias => {
                    sql.close();
                    res.status(200);
                    res.render('carousel-view', { data: carousels.recordset, count: count.recordset, divisions: divisions.recordset, multimedias: multimedias.recordset, 'user': req.session.data, 'permissions': req.session.permissions, code: 200 });
                }).catch(err => {
                    console.log(err);
                    res.status(500);
                    res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
                });
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post("/", auth, (req, res) => {
    var division = req.body.division;
    var multimedia = req.body.multimedia;
    var interval = req.body.interval;
    var status;
    if (req.body.status) {
        status = req.body.status;
    } else {
        status = false;
    }
    (async function() {
        try {
            let pool = await sql.connect(config);
            let result = await pool.request()
                .input('fk_division', sql.Int, division)
                .query("SELECT id FROM tbl_cat_carousel WHERE fk_division = @fk_division");
            if (result.recordset.length != 0) {
                let result = await pool.request()
                    .input('fk_division', sql.Int, division)
                    .query("DELETE FROM tbl_cat_carousel WHERE fk_division = @fk_division");
                if (multimedia != null) {
                    for (let i = 0; i < multimedia.length; i++) {
                        let result1 = await pool.request()
                            .input('fk_division', sql.Int, division)
                            .input('fk_multimedia', sql.Int, multimedia[i])
                            .input('position', sql.Int, i)
                            .input('interval', sql.VarChar, interval[i])
                            .input('status', sql.Int, status)
                            .query('INSERT INTO tbl_cat_carousel (fk_division, fk_multimedia, position, interval, status) ' +
                                'VALUES (@fk_division, @fk_multimedia, @position, @interval, @status)');
                    }
                }
            } else {
                for (let i = 0; i < multimedia.length; i++) {
                    let result1 = await pool.request()
                        .input('fk_division', sql.Int, division)
                        .input('fk_multimedia', sql.Int, multimedia[i])
                        .input('position', sql.Int, i)
                        .input('interval', sql.VarChar, interval[i])
                        .input('status', sql.Int, status)
                        .query('INSERT INTO tbl_cat_carousel (fk_division, fk_multimedia, position, interval, status) ' +
                            'VALUES (@fk_division, @fk_multimedia, @position, @interval, @status)');
                }
            }
            let result2 = await pool.request()
                .query("SELECT C.id AS id, D.name AS division, C.position AS position, C.interval AS interval, M.name AS name, M.link AS link, M.type AS type, M.id AS multimedia, FORMAT(C.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(C.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(C.status=1,'Activo','Inactivo') AS status FROM tbl_cat_carousel C INNER JOIN tbl_cat_multimedia_files M ON M.id = C.fk_multimedia INNER JOIN tbl_cat_divisions D ON D.id = C.fk_division");
            let result3 = await pool.request()
                .query("SELECT fk_division FROM tbl_cat_carousel GROUP BY fk_division");
            let result4 = await pool.request()
                .query("SELECT id, name FROM tbl_cat_divisions WHERE status = 1");
            let result5 = await pool.request()
                .query("SELECT id, name, type, link FROM tbl_cat_multimedia_files WHERE status = 1");
            sql.close();
            res.status(201);
            res.render('carousel-view', { data: result2.recordset, count: result3.recordset, divisions: result4.recordset, multimedias: result5.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha guardado el carrusel', code: 201 });
        } catch (err) {
            sql.close();
            console.log(err);
            res.status(500);
            res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        }
    })();
    /*(async function() {
        try {
            let pool = await sql.connect(config);
            for (let i = 0; i < multimedia.length; i++) {
                let result = await pool.request()
                    .input('fk_division', sql.Int, division)
                    .input('fk_multimedia', sql.Int, multimedia[i])
                    .input('position', sql.Int, i)
                    .input('interval', sql.VarChar, interval[i])
                    .input('status', sql.Int, status)
                    .query('INSERT INTO tbl_cat_carousel (fk_division, fk_multimedia, position, interval, status) ' +
                        'VALUES (@fk_division, @fk_multimedia, @position, @interval, @status)');
            }
            let result2 = await pool.request()
                .query("SELECT C.id AS id, D.name AS division, C.position AS position, C.interval AS interval, M.name AS name, M.link AS link, M.type AS type, FORMAT(C.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(C.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(C.status=1,'Activo','Inactivo') AS status FROM tbl_cat_carousel C INNER JOIN tbl_cat_multimedia_files M ON M.id = C.fk_multimedia INNER JOIN tbl_cat_divisions D ON D.id = C.fk_division");
            let result3 = await pool.request()
                .query("SELECT fk_division FROM tbl_cat_carousel GROUP BY fk_division");
            let result4 = await pool.request()
                .query("SELECT id, name FROM tbl_cat_divisions WHERE status = 1");
            let result5 = await pool.request()
                .query("SELECT id, name, type, link FROM tbl_cat_multimedia_files WHERE status = 1");
            sql.close();
            res.status(201);
            res.render('carousel-view', { data: result2.recordset, count: result3.recordset, divisions: result4.recordset, multimedias: result5.recordset, message: 'Se ha creado el nuevo carrusel', code: 201 });
        } catch (err) {
            sql.close();
            console.log(err);
            res.status(500);
            res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        }
    })();*/
});

router.get('/search', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT C.status, C.id AS id, D.name AS division, C.fk_division AS fk_division, C.fk_multimedia AS fk_multimedia,C.position AS position, C.interval AS interval, M.name AS name, M.link AS link, M.type AS type FROM tbl_cat_carousel C INNER JOIN tbl_cat_multimedia_files M ON M.id = C.fk_multimedia INNER JOIN tbl_cat_divisions D ON D.id = C.fk_division WHERE C.id = @id");
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT id, name FROM tbl_cat_divisions WHERE status = 1");
        }).then(divisions => {
            sql.connect(config).then(pool => {
                return pool.request().query("SELECT id, name, type, link FROM tbl_cat_multimedia_files WHERE status = 1");
            }).then(multimedias => {
                sql.close();
                res.status(200);
                res.send({ data: result.recordset, divisions: divisions.recordset, multimedias: multimedias.recordset, code: 200 });
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post('/update', auth, (req, res) => {
    var id = req.body.id;
    var division = req.body.division;
    var multimedia = req.body.multimedia;
    var interval = req.body.interval;
    let status;
    if (req.body.status) {
        status = req.body.status;
    } else {
        status = false;
    }
    (async function() {
        try {
            let pool = await sql.connect(config);
            for (let i = 0; i < multimedia.length; i++) {
                let result = await pool.request()
                    .input('id', sql.Int, id)
                    .input('fk_division', sql.Int, division)
                    .input('fk_multimedia', sql.Int, multimedia)
                    .input('interval', sql.VarChar, interval)
                    .input('modification_date', sql.DateTimeOffset, new Date())
                    .input('status', sql.Bit, status)
                    .query('UPDATE tbl_cat_carousel set fk_division = @fk_division, fk_multimedia = @fk_multimedia, interval = @interval, modification_date = @modification_date, status = @status where id = @id');
            }
            let result2 = await pool.request()
                .query("SELECT C.id AS id, D.name AS division, C.position AS position, C.interval AS interval, M.name AS name, M.link AS link, M.type AS type, M.id AS multimedia, FORMAT(C.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(C.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(C.status=1,'Activo','Inactivo') AS status FROM tbl_cat_carousel C INNER JOIN tbl_cat_multimedia_files M ON M.id = C.fk_multimedia INNER JOIN tbl_cat_divisions D ON D.id = C.fk_division");
            let result3 = await pool.request()
                .query("SELECT fk_division FROM tbl_cat_carousel GROUP BY fk_division");
            let result4 = await pool.request()
                .query("SELECT id, name FROM tbl_cat_divisions WHERE status = 1");
            let result5 = await pool.request()
                .query("SELECT id, name, type, link FROM tbl_cat_multimedia_files WHERE status = 1");
            sql.close();
            res.status(201);
            res.render('carousel-view', { data: result2.recordset, count: result3.recordset, divisions: result4.recordset, multimedias: result5.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha actualizado el carrusel', code: 201 });
        } catch (err) {
            sql.close();
            console.log(err);
            res.status(500);
            res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        }
    })();
});

router.get('/search/division', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('fk_division', sql.Int, id)
            .query("SELECT id FROM tbl_cat_carousel WHERE fk_division = @fk_division");
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request()
                .query("SELECT id, name, link, type FROM tbl_cat_multimedia_files WHERE status = 1");
        }).then(multimedia => {
            sql.close();
            res.status(200);
            res.send({ data: result.recordset, multimedia: multimedia.recordset, code: 200 });
        }).catch(err => {
            sql.close();
            console.log(err);
            res.status(500);
            res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        sql.close();
        console.log(err);
        res.status(500);
        res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.get('/search/division/id/', auth, (req, res) => {
    var id = req.query.id;
    let status = true;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id_division', sql.Int, id)
            .input('status', sql.Bit, status)
            .query("SELECT C.id AS id, D.name AS division, C.position AS position, C.interval AS interval, C.fk_multimedia AS fk_multimedia,M.name AS name, M.link AS link, M.type AS type, C.status AS status FROM tbl_cat_carousel C INNER JOIN tbl_cat_multimedia_files M ON M.id = C.fk_multimedia INNER JOIN tbl_cat_divisions D ON D.id = C.fk_division WHERE D.id = @id_division AND C.status = @status AND M.status = 1");
    }).then(carousels => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT id, name, type, link FROM tbl_cat_multimedia_files WHERE status = 1");
        }).then(multimedias => {
            sql.close();
            res.status(200);
            res.send({ carousels: carousels.recordset, multimedias: multimedias.recordset, code: 200 });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('carousel-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.get('/download', auth, (req, res) => {
    var file = '';
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT link FROM tbl_cat_multimedia_files WHERE status = 1 AND id = @id");
    }).then(result => {
        res.status(200);
        result.recordset.forEach(function(value, indice, array) {
            file = __dirname + '/../../../public/' + value.link;
        });
        res.download(file);
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

module.exports = router;