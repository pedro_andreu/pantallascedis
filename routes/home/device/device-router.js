var express = require('express');
var router = express.Router();

let DATA = require('../../../public/class/tools/data.js');

var sql = require('mssql');

sql.on('error', err => {
    console.log(err);
});

var config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};

var auth = (req, res, next) => {
    if (req.session && req.session.user !== null && req.session.admin) {
        return next();
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.VIEW_LOGIN, { code: DATA.UNAUTHORIZED });
    }
};

router.get('/', auth, (req, res) => {
    sql.connect(config).then(pool => {
        return pool.request().query("SELECT DE.id, DE.ip, DI.name, FORMAT(DE.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(DE.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(DE.status=1,'Activo','Inactivo') AS status FROM tbl_cat_devices DE INNER JOIN tbl_cat_divisions DI ON DI.id = DE.fk_division");
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query('SELECT * FROM tbl_cat_divisions WHERE status = 1');
        }).then(resultQ => {
            res.status(200);
            res.render('device-view', { data: result.recordset, divisions: resultQ.recordset, 'user': req.session.data, 'permissions': req.session.permissions, code: 200 });
        }).then(() => {
            return sql.close();
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post("/", auth, (req, res) => {
    var ip = req.body.ip;
    var division = req.body.division;
    var status = req.body.status;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('ip', sql.VarChar, ip)
            .input('fk_division', sql.Int, division)
            .input('status', sql.Int, status)
            .query('INSERT INTO tbl_cat_devices (ip, fk_division, status) VALUES (@ip, @fk_division, @status)');
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT DE.id, DE.ip, DI.name, FORMAT(DE.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(DE.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(DE.status=1,'Activo','Inactivo') AS status FROM tbl_cat_devices DE INNER JOIN tbl_cat_divisions DI ON DI.id = DE.fk_division");
        }).then(result => {
            sql.connect(config).then(pool => {
                return pool.request().query('SELECT * FROM tbl_cat_divisions WHERE status = 1');
            }).then(resultQ => {
                res.status(201);
                res.render('device-view', { data: result.recordset, divisions: resultQ.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha creado el nuevo dispositivo', code: 201 });
            }).then(() => {
                return sql.close();
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    });
});

router.get('/search', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT DE.id, DE.ip, DE.fk_division, DI.name, DE.status FROM tbl_cat_devices DE INNER JOIN tbl_cat_divisions DI ON DI.id = DE.fk_division WHERE DE.id = @id");
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT id, name FROM tbl_cat_divisions WHERE status = 1");
        }).then(resultQ => {
            res.status(200);
            res.send({ data: result.recordset, divisions: resultQ.recordset, code: 200 });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post('/update', auth, (req, res) => {
    var id = req.body.id;
    var ip = req.body.ip;
    var division = req.body.division;
    var status;
    if (req.body.status) {
        status = req.body.status;
    } else {
        status = false;
    }
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .input('ip', sql.VarChar, ip)
            .input('fk_division', sql.Int, division)
            .input('modification_date', sql.DateTimeOffset, new Date())
            .input('status', sql.Bit, status)
            .query('UPDATE tbl_cat_devices SET ip = @ip, fk_division = @fk_division, modification_date = @modification_date, status = @status WHERE id = @id');
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT DE.id, DE.ip, DI.name, FORMAT(DE.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(DE.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(DE.status=1,'Activo','Inactivo') AS status FROM tbl_cat_devices DE INNER JOIN tbl_cat_divisions DI ON DI.id = DE.fk_division");
        }).then(resultDevices => {
            sql.connect(config).then(pool => {
                return pool.request().query('SELECT * FROM tbl_cat_divisions WHERE status = 1');
            }).then(resultDivisions => {
                res.status(200);
                res.render('device-view', { data: resultDevices.recordset, divisions: resultDivisions.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha actualizado el nuevo dispositivo', code: 200 });
            }).then(() => {
                return sql.close();
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('device-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

module.exports = router;