var express = require('express');
var router = express.Router();

let DATA = require('../../../public/class/tools/data.js');

var sql = require('mssql');

sql.on('error', err => {
    console.log(err);
});

var config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};

var auth = (req, res, next) => {
    if (req.session && req.session.user !== null && req.session.admin) {
        return next();
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.VIEW_LOGIN, { code: DATA.UNAUTHORIZED });
    }
};

router.get('/', auth, (req, res) => {
    sql.connect(config).then(pool => {
        return pool.request().query("SELECT id, name, FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_divisions");
    }).then(result => {
        res.status(200);
        res.render('division-view', { data: result.recordset, 'user': req.session.data, 'permissions': req.session.permissions, code: 200 });
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('division-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post("/", auth, (req, res) => {
    var name = req.body.name;
    var status = req.body.status;
    sql.connect(config).then(pool => {
            return pool.request()
                .input('name', sql.VarChar, name)
                .input('status', sql.Int, status)
                .query('INSERT INTO tbl_cat_divisions (name, status) VALUES (@name, @status)');
        }).then(result => {
            sql.connect(config).then(pool => {
                return pool.request().query("SELECT id, name, FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_divisions");
            }).then(resultQ => {
                res.status(201);
                res.render('division-view', { data: resultQ.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha creado la nueva división', code: 201 });
            }).then(() => {
                return sql.close();
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('division-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500);
            res.render('division-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
});

router.get('/search', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT id, name, status FROM tbl_cat_divisions WHERE id = @id");
    }).then(result => {
        res.status(200);
        res.send({ data: result.recordset, code: 200 });
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('division-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post('/update', auth, (req, res) => {
    var id = req.body.id;
    var name = req.body.name;
    var status;
    if (req.body.status) {
        status = req.body.status;
    } else {
        status = false;
    }
    sql.connect(config).then(pool => {
            return pool.request()
                .input('id', sql.Int, id)
                .input('name', sql.VarChar, name)
                .input('modification_date', sql.DateTimeOffset, new Date())
                .input('status', sql.Bit, status)
                .query('UPDATE tbl_cat_divisions SET name = @name, modification_date = @modification_date, status = @status WHERE id = @id');
        }).then(result => {
            sql.connect(config).then(pool => {
                return pool.request().query("SELECT id, name, FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_divisions");
            }).then(resultQ => {
                res.status(200);
                res.render('division-view', { data: resultQ.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha actualizado la división', code: 200 });
            }).then(() => {
                return sql.close();
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('division-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500);
            res.render('division-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
});

module.exports = router;