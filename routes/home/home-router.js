let express = require('express');
let session = require('express-session');

let Permission = require('../../public/class/db/permission.js');
let User = require('../../public/class/db/user.js');
const DATA = require('../../public/class/tools/data.js');

let router = express.Router();

let user = new User();
let permission = new Permission();

var auth = (req, res, next) => {
    if (req.session && req.session.user !== null && req.session.admin) {
        return next();
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.LOGIN_VIEW, { code: DATA.UNAUTHORIZED });
    }
};

router.get('/', auth, (req, res) => {
    let id = req.session.user;
    if (id != null) {
        user.findUserById(id).then(user => {
            if (user.length != 0) {
                permission.findPermissionByUserId(id)
                    .then(permissions => {
                        if (permissions.length != 0) {
                            req.session.data = user;
                            req.session.permissions = permissions;
                            res.status(DATA.OK);
                            res.render(DATA.HOME_VIEW, { 'user': user, 'permissions': permissions, code: DATA.OK });
                        } else {
                            req.session.data = user;
                            res.status(DATA.OK);
                            res.render(DATA.HOME_VIEW, { 'user': user, code: DATA.OK });
                        }
                    }).catch(err => {
                        console.log(err);
                        res.status(DATA.INTERNAL_SERVER_ERROR);
                        res.render(DATA.LOGIN_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
                    });
            } else {
                res.status(DATA.UNAUTHORIZED);
                res.render(DATA.LOGIN_VIEW, { message: DATA.NO_RESULT_MESSAGE, code: DATA.UNAUTHORIZED });
            }
        }).catch(err => {
            console.log(err);
            res.status(DATA.INTERNAL_SERVER_ERROR);
            res.render(DATA.LOGIN_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
        });
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.LOGIN_VIEW, { message: DATA.NO_RESULT_MESSAGE, code: DATA.UNAUTHORIZED });
    }
});

module.exports = router;