var express = require('express');
var router = express.Router();

let DATA = require('../../../public/class/tools/data.js');

var sql = require('mssql');
var fs = require('fs');

sql.on('error', err => {
    console.log(err);
});

var config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};

var auth = (req, res, next) => {
    if (req.session && req.session.user !== null && req.session.admin) {
        return next();
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.VIEW_LOGIN, { code: DATA.UNAUTHORIZED });
    }
};

router.get('/', auth, (req, res) => {
    sql.connect(config).then(pool => {
        return pool.request().query("SELECT id, name, type, format, mimetype, size, link ,FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_multimedia_files");
    }).then(result => {
        res.status(200);
        res.render('multimedia-view', { data: result.recordset, 'user': req.session.data, 'permissions': req.session.permissions, code: 200 });
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post("/", auth, (req, res) => {
    var name = req.body.name;
    var multimedia = req.files.multimedia;
    var ext = multimedia.name.split('.').pop();
    var size = trunc(multimedia.size / 1024, 3) + " KB";
    var status = req.body.status;
    var mimetype = req.files.multimedia.mimetype;
    var type = '';
    if (ext == 'jpg' | ext == 'jpeg' | ext == 'png' | ext == 'gif' | ext == 'PNG' | ext == 'JPG') {
        type = 'imagen';
    } else if (ext == 'mp4' | ext == 'avi' | ext == 'mkv') {
        type = 'video';
    } else {
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    }
    multimedia.mv('./public/uploads/' + multimedia.name, function(err) {
        if (err) {
            console.log(err);
        } else {
            var newname = name + '.' + ext;
            fs.rename('./public/uploads/' + multimedia.name, './public/uploads/' + newname, function(err) {
                if (err) {
                    console.log(err);
                } else {
                    sql.connect(config).then(pool => {
                        return pool.request()
                            .input('name', sql.VarChar, newname)
                            .input('type', sql.VarChar, type)
                            .input('format', sql.VarChar, ext)
                            .input('mimetype', sql.VarChar, mimetype)
                            .input('size', sql.VarChar, size)
                            .input('link', sql.VarChar, '/uploads/' + newname)
                            .input('status', sql.Int, status)
                            .query('INSERT INTO tbl_cat_multimedia_files (name, type, format, mimetype, size, link, status) VALUES (@name, @type, @format, @mimetype, @size, @link, @status)');
                    }).then(resultI => {
                        sql.connect(config).then(pool => {
                            return pool.request().query("SELECT id, name, type, format, mimetype, size, link ,FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_multimedia_files");
                        }).then(result => {
                            res.status(201);
                            res.render('multimedia-view', { data: result.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha creado el nuevo archivo multimedia', code: 201 });
                        }).then(() => {
                            return sql.close();
                        }).catch(err => {
                            console.log(err);
                            res.status(500);
                            res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
                        });
                    });
                }
            });
        }
    });
});

router.get('/search', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT id, name, link, type, status FROM tbl_cat_multimedia_files WHERE id = @id");
    }).then(result => {
        res.status(200);
        res.send({ data: result.recordset, code: 200 });
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.get('/search/all', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .query("SELECT id, name, link, type FROM tbl_cat_multimedia_files WHERE status = 1");
    }).then(result => {
        sql.close();
        res.status(200);
        res.send({ data: result.recordset, code: 200 });
    }).catch(err => {
        sql.close();
        console.log(err);
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post('/update', auth, (req, res) => {
    var id = req.body.id;
    var oldName = '';
    let status;
    if (req.body.status) {
        status = req.body.status;
    } else {
        status = false;
    }
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT id, name, link FROM tbl_cat_multimedia_files WHERE id = @id");
    }).then(result => {
        res.status(200);
        oldName = result.recordset[0].link;
        var name = req.body.name;
        if (req.files !== null) {
            var multimedia = req.files.multimedia;
            var ext = multimedia.name.split('.').pop();
            var size = trunc(multimedia.size / 1024, 3) + " KB";
            var mimetype = req.files.multimedia.mimetype;
            var type = '';
            if (ext == 'jpg' | ext == 'jpeg' | ext == 'png' | ext == 'gif' | ext == 'PNG' | ext == 'JPG') {
                type = 'imagen';
            } else if (ext == 'mp4' | ext == 'avi' | ext == 'mkv') {
                type = 'video';
            } else {
                res.status(500);
                res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            }
            multimedia.mv('./public/uploads/' + multimedia.name, function(err) {
                if (err) {
                    console.log(err);
                } else {
                    var newname = name + '.' + ext;
                    var fs = require('fs');
                    fs.rename('./public/uploads/' + multimedia.name, './public/uploads/' + newname, function(err) {
                        if (err) {
                            console.log(err);
                        } else {
                            sql.connect(config).then(pool => {
                                return pool.request()
                                    .input('id', sql.Int, id)
                                    .input('name', sql.VarChar, name)
                                    .input('type', sql.VarChar, type)
                                    .input('format', sql.VarChar, ext)
                                    .input('mimetype', sql.VarChar, mimetype)
                                    .input('size', sql.VarChar, size)
                                    .input('link', sql.VarChar, '/uploads/' + newname)
                                    .input('modification_date', sql.DateTimeOffset, new Date())
                                    .input('status', sql.Bit, status)
                                    .query('update tbl_cat_multimedia_files set name = @name, type = @type, format = @format, mimetype = @mimetype, size = @size, link = @link, modification_date = @modification_date, status = @status where id = @id');
                            }).then(resultI => {
                                sql.connect(config).then(pool => {
                                    return pool.request().query("SELECT id, name, type, format, mimetype, size, link ,FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_multimedia_files");
                                }).then(result => {
                                    res.status(201);
                                    res.render('multimedia-view', { data: result.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha actualizado el archivo multimedia', code: 201 });
                                }).then(() => {
                                    return sql.close();
                                }).catch(err => {
                                    console.log(err);
                                    res.status(500);
                                    res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
                                });
                            });
                        }
                    });
                }
            });
        } else {
            var spLink = oldName.split('.');
            var newLink = '/uploads/' + name + '.' + spLink[1];
            var oldLink = './public' + oldName;
            var fs = require('fs');
            fs.rename('./public' + oldName, './public' + newLink, function(err) {
                if (err) console.log('ERROR: ' + err);
            });
            sql.connect(config).then(pool => {
                return pool.request()
                    .input('id', sql.Int, id)
                    .input('name', sql.VarChar, name)
                    .input('mimetype', sql.VarChar, mimetype)
                    .input('link', sql.VarChar, newLink)
                    .input('modification_date', sql.DateTimeOffset, new Date())
                    .input('status', sql.Bit, status)
                    .query('update tbl_cat_multimedia_files set name = @name, modification_date = @modification_date, status = @status, link = @link where id = @id');
            }).then(resultI => {
                sql.connect(config).then(pool => {
                    return pool.request().query("SELECT id, name, type, format, mimetype, size, link ,FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_multimedia_files");
                }).then(result => {
                    res.status(201);
                    res.render('multimedia-view', { data: result.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha actualizado el archivo multimedia', code: 201 });
                }).then(() => {
                    return sql.close();
                }).catch(err => {
                    console.log(err);
                    res.status(500);
                    res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
                });
            });
        }
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.get('/download', auth, (req, res) => {
    var file = '';
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT link FROM tbl_cat_multimedia_files WHERE status = 1 AND id = @id");
    }).then(result => {
        res.status(200);
        result.recordset.forEach(function(value, indice, array) {
            file = __dirname + '/../../../public/' + value.link;
        });
        res.download(file);
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('multimedia-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

function trunc(x, posiciones = 0) {
    var s = x.toString();
    var l = s.length;
    var decimalLength = s.indexOf('.') + 1;
    var numStr = s.substr(0, decimalLength + posiciones);
    return Number(numStr);
}

module.exports = router;