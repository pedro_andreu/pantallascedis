var express = require('express');

let router = express.Router();
let Permission = require('../../../public/class/db/permission.js');
let DATA = require('../../../public/class/tools/data.js');

let permission = new Permission();

router.get('/', (req, res) => {
    permission.getPermissions()
        .then(permissions => {
            res.status(DATA.OK);
            res.send({ permissions: permissions, code: DATA.OK });
        }).catch(err => {
            console.log(err);
            res.status(DATA.INTERNAL_SERVER_ERROR);
            res.render(DATA.VIEW_ROLE, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
        });
});






const sql = require('mssql');



const config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};
/*
router.get('/', (req, res) => {
    sql.connect(config).then(pool => {
        return pool.request().query('SELECT id, name from tbl_cat_permissions WHERE status = 1');
    }).then(result => {
        res.send({ data: result.recordset });
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
    });
});
*/
router.get('/search', (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query('SELECT P.name FROM tbl_cat_roles R INNER JOIN tbl_rel_roles_permissions RP ON R.id = RP.fk_role INNER JOIN tbl_cat_permissions P ON P.id = RP.fk_permission WHERE R.id = @id');
    }).then(permissions => {
        res.send({ permissions: permissions.recordset });
    }).then(() => {
        return sql.close();
    }).catch(err => {
        console.log(err);
    });
});

module.exports = router;