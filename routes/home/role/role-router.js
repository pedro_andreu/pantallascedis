let express = require('express');
let session = require('express-session');

let router = express.Router();
let Role = require('../../../public/class/db/role.js');
const DATA = require('../../../public/class/tools/data.js');

let role = new Role();

var auth = (req, res, next) => {
    if (req.session && req.session.user !== null && req.session.admin) {
        return next();
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.LOGIN_VIEW, { code: DATA.UNAUTHORIZED });
    }
};

router.get('/', auth, (req, res) => {
    role.getRoles()
        .then(roles => {
            res.status(DATA.OK);
            res.render(DATA.ROLE_VIEW, { roles: roles, 'user': req.session.data, 'permissions': req.session.permissions, code: DATA.OK });
        }).catch(err => {
            console.log(err);
            res.status(DATA.INTERNAL_SERVER_ERROR);
            res.render(DATA.ROLE_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
        });
});

router.post("/", auth, (req, res) => {
    let name = req.body.name;
    let status = req.body.status;
    let permissions = req.body.permission;
    role.createRole(name, status)
        .then(id => {
            if (permissions != null) {
                role.createRolePermission(id, permissions)
                    .then(result => {
                        role.getRoles()
                            .then(roles => {
                                res.status(DATA.CREATED);
                                res.render(DATA.ROLE_VIEW, { roles: roles, 'user': req.session.data, 'permissions': req.session.permissions, message: DATA.CREATED_ROLE_MESSAGE, code: DATA.CREATED });
                            }).catch(err => {
                                console.log(err);
                                res.status(DATA.INTERNAL_SERVER_ERROR);
                                res.render(DATA.ROLE_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
                            });
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(DATA.INTERNAL_SERVER_ERROR);
                        res.render(DATA.ROLE_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
                    });
            } else {
                role.getRoles()
                    .then(roles => {
                        res.status(DATA.CREATED);
                        res.render(DATA.ROLE_VIEW, { roles: roles, message: DATA.CREATED_ROLE_MESSAGE, code: DATA.CREATED });
                    }).catch(err => {
                        console.log(err);
                        res.status(DATA.INTERNAL_SERVER_ERROR);
                        res.render(DATA.ROLE_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
                    });
            }
        }).catch(err => {
            console.log(err);
            res.status(DATA.INTERNAL_SERVER_ERROR);
            res.render(DATA.ROLE_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
        });
});

router.get('/search', (req, res) => {
    let id = req.query.id;
    role.findRoleById(id)
        .then(role => {
            if (role != null) {
                console.log(role);
                findPermissionsByRole(id)
                    .then(result => {
                        console.log(result);
                        res.send({ role: role.recordset, code: 200 });
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(DATA.INTERNAL_SERVER_ERROR);
                        res.render(DATA.VIEW_ROLE, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
                    });
            } else {
                // Si no tiene roles
            }
        }).catch(err => {
            console.log(err);
            res.status(DATA.INTERNAL_SERVER_ERROR);
            res.render(DATA.VIEW_ROLE, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
        });
});




let sql = require('mssql');
const { restart } = require('nodemon');

const config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};

router.get('/search', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT id, name, status FROM tbl_cat_roles WHERE id = @id");
    }).then(role => {
        sql.connect(config).then(pool => {
            return pool.request()
                .input('id', sql.Int, id)
                .query('SELECT P.id, P.name FROM tbl_cat_roles R INNER JOIN tbl_rel_roles_permissions RP ON R.id = RP.fk_role INNER JOIN tbl_cat_permissions P ON P.id = RP.fk_permission WHERE R.id = @id');
        }).then(permissionsrel => {
            sql.connect(config).then(pool => {
                return pool.request()
                    .query('SELECT id, name FROM tbl_cat_permissions');
            }).then(permissions => {
                res.status(200);
                res.send({ role: role.recordset, permissionsrel: permissionsrel.recordset, permissions: permissions.recordset, code: 200 });
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('role-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('role-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('role-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post('/update', auth, (req, res) => {
    var id = req.body.id;
    var name = req.body.name;
    var status;
    if (req.body.status) {
        status = req.body.status;
    } else {
        status = false;
    }
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .input('name', sql.VarChar, name)
            .input('modification_date', sql.DateTimeOffset, new Date())
            .input('status', sql.Bit, status)
            .query('UPDATE tbl_cat_roles SET name = @name, modification_date = @modification_date, status = @status WHERE id = @id');
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request()
                .input('id', sql.Int, id)
                .query('DELETE FROM tbl_rel_roles_permissions WHERE fk_role = @id');
        }).then(result => {
            sql.connect(config).then(pool => {
                getPermissions(req).forEach(function(valor, indice, array) {
                    return pool.request()
                        .input('fk_role', sql.Int, id)
                        .input('fk_permission', sql.Int, valor)
                        .query('INSERT INTO tbl_rel_roles_permissions (fk_role, fk_permission) VALUES (@fk_role, @fk_permission)');
                });
            }).then(resultDivisions => {
                sql.connect(config).then(pool => {
                    return pool.request().query("SELECT id, name, FORMAT(creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(status=1,'Activo','Inactivo') AS status FROM tbl_cat_roles");
                }).then(roles => {
                    res.status(201);
                    res.render('role-view', { roles: roles.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha actualizado el rol', code: 201 });
                }).then(() => {
                    return sql.close();
                }).catch(err => {
                    console.log(err);
                    res.status(500);
                    res.render('role-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
                });
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('role-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('role-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('role-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });

    getPermissions(req).forEach(function(valor, indice, array) {

    });
});

function getPermissions(req) {
    var permissions = [];
    if (req.body.permission1 != null) {
        permissions.push(req.body.permission1);
    }
    if (req.body.permission2 != null) {
        permissions.push(req.body.permission2);
    }
    if (req.body.permission3 != null) {
        permissions.push(req.body.permission3);
    }
    if (req.body.permission4 != null) {
        permissions.push(req.body.permission4);
    }
    if (req.body.permission5 != null) {
        permissions.push(req.body.permission5);
    }
    if (req.body.permission6 != null) {
        permissions.push(req.body.permission6);
    }
    return permissions;
}

module.exports = router;