var express = require('express');
var router = express.Router();

let DATA = require('../../../public/class/tools/data.js');

var sql = require('mssql');

sql.on('error', err => {
    console.log(err);
});

var config = {
    server: 'SPINS107134\\SQLExpress',
    database: 'ticedis',
    user: 'sa',
    password: 'SQLS2020_red$P381',
    "options": {
        "encrypt": true,
        "enableArithAbort": true
    },
    port: 1433
};

var auth = (req, res, next) => {
    if (req.session && req.session.user !== null && req.session.admin) {
        return next();
    } else {
        res.status(DATA.UNAUTHORIZED);
        res.render(DATA.VIEW_LOGIN, { code: DATA.UNAUTHORIZED });
    }
};

router.get('/', auth, (req, res) => {
    sql.connect(config).then(pool => {
        return pool.request().query("SELECT U.id, U.name, U.first_name, U.last_name, U.email, R.name AS role, FORMAT(U.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(U.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(U.status=1,'Activo','Inactivo') AS status FROM tbl_cat_users U INNER JOIN tbl_cat_roles R ON R.id = U.fk_role");
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query('SELECT * FROM tbl_cat_roles WHERE status = 1');
        }).then(resultQ => {
            res.status(200);
            res.render('user-view', { data: result.recordset, roles: resultQ.recordset, 'user': req.session.data, 'permissions': req.session.permissions });
        }).then(() => {
            return sql.close();
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post("/", auth, (req, res) => {
    var name = req.body.name;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var email = req.body.email;
    var rol = req.body.rol;
    var status = req.body.status;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('name', sql.VarChar, name)
            .input('first_name', sql.VarChar, first_name)
            .input('last_name', sql.VarChar, last_name)
            .input('email', sql.VarChar, email)
            .input('fk_role', sql.Int, rol)
            .input('status', sql.Int, status)
            .query('INSERT INTO tbl_cat_users (name, first_name, last_name, email, fk_role, status) VALUES (@name, @first_name, @last_name, @email, @fk_role, @status)');
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT U.id, U.name, U.first_name, U.last_name, U.email, R.name AS role, FORMAT(U.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(U.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(U.status=1,'Activo','Inactivo') AS status FROM tbl_cat_users U INNER JOIN tbl_cat_roles R ON R.id = U.fk_role");
        }).then(result => {
            sql.connect(config).then(pool => {
                return pool.request().query('SELECT * FROM tbl_cat_roles WHERE status = 1');
            }).then(resultQ => {
                res.status(201);
                res.render('user-view', { data: result.recordset, roles: resultQ.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha creado el nuevo usuario' });
            }).then(() => {
                return sql.close();
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    });
});

router.get('/search', auth, (req, res) => {
    var id = req.query.id;
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .query("SELECT U.id, U.name, U.first_name, U.last_name, U.email, U.fk_role, R.name AS role, U.status FROM tbl_cat_users U INNER JOIN tbl_cat_roles R ON R.id = U.fk_role Where U.id = @id");
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT id, name FROM tbl_cat_roles WHERE status = 1");
        }).then(resultQ => {
            res.status(200);
            res.send({ data: result.recordset, roles: resultQ.recordset, code: 200 });
        }).then(() => {
            return sql.close();
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

router.post('/update', auth, (req, res) => {
    var id = req.body.id;
    var name = req.body.name;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var email = req.body.email;
    var role = req.body.role;
    var status;
    if (req.body.status) {
        status = req.body.status;
    } else {
        status = false;
    }
    sql.connect(config).then(pool => {
        return pool.request()
            .input('id', sql.Int, id)
            .input('name', sql.VarChar, name)
            .input('first_name', sql.VarChar, first_name)
            .input('last_name', sql.VarChar, last_name)
            .input('email', sql.VarChar, email)
            .input('fk_role', sql.Int, role)
            .input('modification_date', sql.DateTimeOffset, new Date())
            .input('status', sql.Bit, status)
            .query('UPDATE tbl_cat_users SET name = @name, first_name = @first_name, last_name = @last_name, email = @email, fk_role = @fk_role, modification_date = @modification_date, status = @status WHERE id = @id');
    }).then(result => {
        sql.connect(config).then(pool => {
            return pool.request().query("SELECT U.id, U.name, U.first_name, U.last_name, U.email, R.name AS role, FORMAT(U.creation_date, 'dd-MM-yyyy hh:mm:ss tt') AS creation_date, FORMAT(U.modification_date, 'dd-MM-yyyy hh:mm:ss tt') AS modification_date, IIF(U.status=1,'Activo','Inactivo') AS status FROM tbl_cat_users U INNER JOIN tbl_cat_roles R ON R.id = U.fk_role");
        }).then(resultUsers => {
            sql.connect(config).then(pool => {
                return pool.request().query('SELECT * FROM tbl_cat_roles WHERE status = 1');
            }).then(resultRoles => {
                res.status(200);
                res.render('user-view', { data: resultUsers.recordset, roles: resultRoles.recordset, 'user': req.session.data, 'permissions': req.session.permissions, message: 'Se ha actualizado el usuario', code: 200 });
            }).then(() => {
                return sql.close();
            }).catch(err => {
                console.log(err);
                res.status(500);
                res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
            });
        }).catch(err => {
            console.log(err);
            res.status(500);
            res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
        });
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.render('user-view', { message: 'Se ha generado un error en la aplicación, contacte al administrador del sistema', code: 500 });
    });
});

module.exports = router;