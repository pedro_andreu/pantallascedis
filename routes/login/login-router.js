let express = require('express');
let session = require('express-session');

let User = require('../../public/class/db/user.js');
const DATA = require('../../public/class/tools/data.js');
let ACtiveDirectory = require('../../public/class/tools/active-directory.js');

let user = new User();
let ad = new ACtiveDirectory();

let router = express.Router();

router.get('/', (req, res) => {
    res.status(DATA.OK);
    res.render(DATA.LOGIN_VIEW);
});

router.post('/authentication', (req, res, next) => {
    let email = req.body.email;
    let password = req.body.password;
    user.findUserByEmail(email)
        .then(user => {
            if (user.length != 0) {
                let id;
                user.forEach((value, indice, array) => {
                    id = value.id;
                });
                ad.findUser(email, password)
                    .then(result => {
                        if (result) {
                            req.session.user = id;
                            req.session.admin = true;
                            res.status(DATA.OK);
                            res.redirect('/home');
                            next();
                        } else {
                            res.status(DATA.UNAUTHORIZED);
                            res.render(DATA.LOGIN_VIEW, { message: DATA.NO_RESULT_MESSAGE, code: DATA.UNAUTHORIZED });
                        }
                    }).catch(err => {
                        console.log(err);
                        res.status(DATA.INTERNAL_SERVER_ERROR);
                        res.render(DATA.LOGIN_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
                    });
            } else {
                res.status(DATA.UNAUTHORIZED);
                res.render(DATA.LOGIN_VIEW, { message: DATA.NO_RESULT_MESSAGE, code: DATA.UNAUTHORIZED });
            }
        }).catch(err => {
            console.log(err);
            res.status(DATA.INTERNAL_SERVER_ERROR);
            res.render(DATA.LOGIN_VIEW, { message: DATA.ERROR_MESSAGE, code: DATA.INTERNAL_SERVER_ERROR });
        });
});

router.get('/logout', (req, res) => {
    req.session.destroy();
    res.status(DATA.OK);
    res.render(DATA.LOGIN_VIEW);
});

module.exports = router;